# -*- coding: utf-8 -*-
#
#  test_drakeutil.py
#  drakeutil
#

"""
Tests for drake helpers.
"""

import unittest
from datetime_tz import datetime_tz as datetime
import os
import sys


class DrakeHelperTest(unittest.TestCase):
    def setUp(self):
        if 'drakeutil' in sys.modules:
            del sys.modules['drakeutil']
        self.du = __import__('drakeutil')

    def test_file_mtime(self):
        expected = datetime.utcfromtimestamp(
                os.stat(__file__).st_mtime
            )
        self.assertEquals(self.du.file_timestamp(__file__), expected)

    def test_file_mtime_missing(self):
        self.assertEquals(self.du.file_timestamp(__file__ + '.unlikely'),
                None)


class DrakeEnvTest(unittest.TestCase):
    def setUp(self):
        os.environ['INPUT'] = 'input'
        os.environ['OUTPUT'] = 'output'
        os.environ['INPUTS'] = 'input0 input1 input2'
        os.environ['OUTPUTS'] = 'output0 output1 output2'
        if 'drakeutil' in sys.modules:
            del sys.modules['drakeutil']
        self.du = __import__('drakeutil')

    def test_special(self):
        self.assertEquals(self.du.INPUT, 'input')
        self.assertEquals(self.du.OUTPUT, 'output')
        self.assertEquals(self.du.INPUTS, ['input0', 'input1', 'input2'])
        self.assertEquals(self.du.OUTPUTS, ['output0', 'output1', 'output2'])


def suite():
    return unittest.TestSuite((
            unittest.makeSuite(DrakeHelperTest),
            unittest.makeSuite(DrakeEnvTest),
        ))


if __name__ == '__main__':
    unittest.TextTestRunner(verbosity=1).run(suite())
